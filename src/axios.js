import axios from 'axios';

axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
axios.defaults.baseURL = 'https://rocky-river-17027.herokuapp.com';
axios.defaults.withCredentials = true;
