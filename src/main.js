import Vue from 'vue'
import App from './App.vue'
import router from './router';
import './axios';
import store from './vuex';

// Import Font Awesome with its library
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

library.add(fas);

Vue.config.productionTip = false

Vue.component('fa', FontAwesomeIcon);

new Vue({
  store,
  router,
  render: h => h(App),
})
    .$mount('#app')
